<?php

namespace GuzzleTest\Model;

use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;

class AsynchronousUploader
{
  public function execute(array $files, string $baseurl, string $dataDir, bool $debug, string $token, int $maxFiles) : int
  {
    $client = new Client();

    $promises = [];
    $uploadCounter = 0;

    foreach ($files as $thisFileName) {

      if (++$uploadCounter>=$maxFiles) {
        break;
      }

      $fullFileName = $dataDir . $thisFileName;
      $file = fopen($fullFileName,'r');

      if ( ! $file ) {
        throw new \Exception('Error opening ' . $fullFileName);
      }

      $thisPromise = $client->requestAsync(
        'POST',
        $baseurl . '/wp/v2/media',
        [
        'body'    => $file,
        'debug'   => $debug,
        'headers' => [
          'Accept'              => 'application/json',
          'Cache-Control'       => 'no-cache',
          'Content-type'        => 'image/jpeg',
          'Content-Disposition' => 'form-data; filename="' . $thisFileName . '"',
          'Authorization'       => 'Bearer ' . $token
          ]
        ]
      );
      $thisPromise->then(
          // $onFulfilled
          function ($response) {
            $picture = json_decode($response->getBody()->getContents());
            echo $picture->slug . " uploaded.\n";
          },
          // $onRejected
          function ($reason) {
              echo "The promise was rejected.\n";
          }
      );

      $promises[] = $thisPromise;
    }

    echo "Starting the event loop\n";
    $pinkySwear = \GuzzleHttp\Promise\all($promises);
    $pinkySwear->wait();

    return $uploadCounter;
  }

}