<?php

namespace GuzzleTest\Model;

use \GuzzleHttp\Client;

class SynchronousUploader
{
  public function execute(array $files, string $baseurl, string $dataDir, bool $debug, string $token, int $maxFiles) : int
  {
    $client = new Client();
    $uploadCounter = 0;

    foreach ($files as $thisFileName) {
      if (++$uploadCounter>=$maxFiles) {
        break;
      }

      $fullFileName = $dataDir . $thisFileName;
      $file = fopen($fullFileName,'r');

      if ( ! $file ) {
        throw new \Exception('Error opening ' . $fullFileName);
      }

      $response = $client->request(
        'POST',
        $baseurl . '/wp/v2/media',
        [
        'body'    => $file,
        'debug'   => $debug,
        'headers' => [
          'Accept'              => 'application/json',
          'Cache-Control'       => 'no-cache',
          'Content-type'        => 'image/jpeg',
          'Content-Disposition' => 'form-data; filename="' . $thisFileName . '"',
          'Authorization'       => 'Bearer ' . $token
          ]
        ]
      );

      if ( $response->getStatusCode() !== 201 ) {
        throw new \Exception($response->getStatusCode() . ' is not the right status code for an upload.');
      }

      $picture = json_decode($response->getBody()->getContents());

    }

    return $uploadCounter;
  }

}