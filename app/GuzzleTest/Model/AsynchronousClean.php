<?php

namespace GuzzleTest\Model;

use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;

class AsynchronousClean
{
  public function execute(
    Client $client,
    string $fileName,
    string $baseurl,
    bool $debug,
    string $token
    )
  {

      $fileName = pathinfo($fileName)['filename'];
      $thisPromise = $client->requestAsync(
        'GET',
        $baseurl . '/wp/v2/media?search=' . $fileName,
        ['headers' => [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
            'Content-type'  => 'application/json',
            ],
          'debug' => $debug
        ]
      );

      $thisPromise->then(
          function ($response) use ($token, $debug, $client) {
            if ($response->getStatusCode() === 200) {
              $pictures = json_decode($response->getBody()->getContents());

              if (count($pictures) < 1) {
                return;
              }

              $response = $client->request(
                'DELETE',
                 $pictures[0]->_links->self[0]->href . '?force=true',
                ['headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept'        => 'application/json',
                    'Content-type'  => 'application/json',
                    ],
                  'debug' => $debug
                ]
              );
            }

          },
          // $onRejected
          function ($response) {
              print_r($reason);
          }
      );
      return $thisPromise;
  }
}
