<?php
namespace GuzzleTest\Traits;

use GuzzleHttp\Client;

trait GetFileList {

  protected $token = '';

  protected function getFileList() : array {
    $dataDir = scandir($this->getApplication()->config['paths']['data']);
    unset($dataDir[0]);
    unset($dataDir[1]);

    return $dataDir;
  }

}
