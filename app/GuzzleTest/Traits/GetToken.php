<?php
namespace GuzzleTest\Traits;

use GuzzleHttp\Client;

trait GetToken {

  protected $token = '';

  protected function getToken() {
    $client = new Client();
    $response = $client->request(
        'POST',
        $this->getApplication()->config['baseurl'] . '/jwt-auth/v1/token',
        [
          'form_params' => [
            'username' => $this->getApplication()->config['credentials']['username'],
            'password' => $this->getApplication()->config['credentials']['password']
          ],
          'headers' => [
              'Accept'     => 'application/json',
          ],
        'debug' => $this->debug

        ]
      );
      $this->token = json_decode($response->getBody(), true);
  }

}
