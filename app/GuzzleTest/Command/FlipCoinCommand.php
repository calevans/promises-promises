<?PHP
namespace GuzzleTest\Command;

use GuzzleTest\Traits\GetToken;
use GuzzleTest\Traits\GetFileList;
use GuzzleTest\Model\AsynchronousClean;
use GuzzleHttp\Promise\RejectionException;

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command
};

use GuzzleHttp\Client;
use GuzzleHttp\Promise\Promise;

class FlipCoinCommand extends Command
{

  protected $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
      $definition = [
        new InputOption('count', 'c', InputOption::VALUE_REQUIRED, 'The number of coins to flip.'),
      ];

      $this->setName('flip')
           ->setDescription('Use Promises to Flips a number of coins.')
           ->setDefinition($definition)
           ->setHelp('This is really stupid command.');
      return;
  }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('Begin Flip Coin', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();

    $count = (int) $input->getOption('count');
    $promises=[];

    for($lcvA=0;$lcvA<=$count;$lcvA++) {

      /*
       * Create my promise with a WAIT function
       */
      $thisPromise = new Promise(
        function () use ( $lcvA, &$thisPromise,$output) {
          $output->writeln("<fg=yellow>Flipping :" . $lcvA . "</>", OutputInterface::VERBOSITY_NORMAL);
          $coin = \random_int(0,1) ? "HEADS" : "TAILS";
          if ($coin === 'TAILS') {
            $thisPromise->resolve([$coin, $output]);
          } else {
            $thisPromise->reject([$coin, $output]);
          }
        }
      );

      /*
       * Add my THEN clauses SUCCESS and FAILURE
       */
      $thisPromise->then(
        function ($params) {
          $params[1]->writeln("We accept <fg=green>" . $params[0] . "</>", OutputInterface::VERBOSITY_NORMAL);
        },
        function ($params)  {
          $params[1]->writeln("We reject <fg=red>" . $params[0] . "</>", OutputInterface::VERBOSITY_NORMAL);
        },
      );

      $promises[] = $thisPromise;
    }

    /*
     * I could do complex and expensive calculations here before resolving or
     * rejecting and given promise or calling it's wait().
     *
     * I could also pass the promise around to other pieces of code and let
     * them resolve/reject.
     *
     * Instead, I'm just going ot call a loop. :)
     */
    foreach ($promises as $thisPromise){
      /*
       * Rejections throw an Exception. That would stop our program.
       */
      try{
        $thisPromise->wait();
      } Catch (RejectionException $e) {
        // NOOP
      }
    }

    $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
    return 0;
  }
}