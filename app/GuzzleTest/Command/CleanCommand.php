<?PHP
namespace GuzzleTest\Command;

use GuzzleTest\Traits\GetToken;
use GuzzleTest\Traits\GetFileList;
use GuzzleTest\Model\AsynchronousClean;

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command
};

use GuzzleHttp\Client;

class CleanCommand extends Command
{
  use GetToken;
  use GetFileList;

  protected $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
      $definition = [];

      $this->setName('clean')
           ->setDescription('remove all the test files from the test site')
           ->setDefinition($definition)
           ->setHelp("Checks to see if any of the test files already exist on the test site and if they do, remove them.");
      return;
  }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('Begin Clean', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();

    $this->getToken($this->debug);

    // get list of files to upload
    $fileList = $this->getFileList();

    $client  = new Client();
    $promiseMaker = new AsynchronousClean();

    $promises = [];

    foreach ($fileList as $fileName) {
      $output->writeln('  Checking ' . $fileName, OutputInterface::VERBOSITY_VERBOSE);

      $promises[] = $promiseMaker->execute($client, $fileName,$this->getApplication()->config['baseurl'],$this->debug,$this->token['token']);
    }

    echo "Starting the event loop\n";
    $pinkySwear = \GuzzleHttp\Promise\all($promises);
    $pinkySwear->wait();

      $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
    return 1;
  }
}