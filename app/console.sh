#!/usr/bin/env php
<?php
require_once __DIR__.'/../vendor/autoload.php';

use GuzzleTest\Command\UploadCommand;
use GuzzleTest\Command\CleanCommand;
use GuzzleTest\Command\FlipCoinCommand;

use Symfony\Component\Console\Application;

$app_path = realpath(dirname(__FILE__) . '/../') . '/';

$config = include $app_path . "/config/config.php";
$app = new Application('GuzzleTest', '1.0.0');

$app->config = $config;

$app->addCommands(
  [
    new UploadCommand(),
    new CleanCommand(),
    new FlipCoinCommand()
  ]
);

$app->run();
