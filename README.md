# Promises, Promises

(c) 2020 [Cal Evans](https://calevans)

This code is released under the MIT license.

This is the source code for the talk Promises, Promises. To fully understand it, find a user group or conference Cal is presenting it at an give it a watch.

To use:

1.  Clone this repo
2.  Run `composer install`
3. Install WordPress on a server somewhere.
4. Install and configure [https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/](https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/)
5. Fill out the `config/config.sample` and rename `config/config.php`
6. cd to the `app` dir
7. execute the command `./console.sh upload -s -c 2`

After each run, make sure you execute `./console clean`

For full debugging, use `-vvv` to see the server conversations.

Enjoy!

Learn!

Share!


